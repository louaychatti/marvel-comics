# Marvel comics 

React native app to show Marvel's characters and comic books using Marvel API

## Functionalities
- Infinity scrollable characters order by name
- Search characters by name
- Character description (If available)
- Character comics list with cover (If available)
- Favorite characters list
    - Character exclusion using swipe list item from right or left


## Technologies
- Typescript
- React Native
- React Navigation
- React Native Paper
- React Vector Icons
- Styled-Components
- Async Storage
- Axios
- MD5


## Project Startup

To run this project, you will need the dependencies bellow:

- Marvel API keys. Click [here](https://developer.marvel.com/) and select "Get a Key" to get a key!
- Android SDK installed with a configured emulator
- XCode installed with a configured emulator **(macOS only)**

Set the Marvel API keys on the file `marvel-app/src/config/api-key.ts`

```
import md5 from 'md5';

const ts = 'marvel-api';
const publicKey = 'public_key';
const privateKey = 'private_key';
const hash = md5(`${ts}${privateKey}${publicKey}`);

const apiKey = {
  ts,
  apikey: publicKey,
  hash,
};

export default apiKey;
```

If you are using NPM run:

```
$ npm install

$ npm run android

or 

$ npm install

$ npx react-native run-android


```

If you are using YARN run:

```
$ yarn install

$ yarn run android

```
