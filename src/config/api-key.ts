import md5 from 'md5';

const ts = 'marvel-api';
const publicKey = 'c87da73a85f6ff9bb1e4b763aefa8786';
const privateKey = 'd607a3b78642d4e97446884789061def0fb7f894';
const hash = md5(`${ts}${privateKey}${publicKey}`);

const apiKey = {
  ts,
  apikey: publicKey,
  hash,
};

export default apiKey;
