import React from 'react';
import { Image} from 'react-native';

import {
  AppDescriptionText,
  AppNameText,
  Container,

} from './styles';

import avengerLogo from '../../assets/images/avengers-logo.png';
import { useTheme } from '../../components/Theme';

const AppInfo: React.FC = () => {
  const { colors } = useTheme();

  return (
    <Container>
      <Image source={avengerLogo} />
      <AppNameText style={{ color: colors.text }}>Marvel Comics</AppNameText>
      <AppDescriptionText style={{ color: colors.primary }}>
        You're missing out the latest comics ?
      </AppDescriptionText>
      <AppDescriptionText style={{ color: colors.text }}>
        Look for your favorite "supe" and check it's latest comics
      </AppDescriptionText>
      
    </Container>
  );
};

export default AppInfo;
