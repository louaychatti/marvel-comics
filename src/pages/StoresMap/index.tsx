import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import React, { Fragment, useEffect, useMemo, useState } from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-vector-icons/Icon';
import { RouteProp } from '@react-navigation/native';
import {  StackNavigationProp } from '@react-navigation/stack';
import { useTheme } from '../../components/Theme';




type RootStackParamList = {
  Home: undefined;
  Profile: {

    screen: React.FC;
  };
  Feed: { sort: 'latest' | 'top' } | undefined;
};

type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Profile'
>;

type ProfileScreenRouteProp = RouteProp<RootStackParamList, 'Profile'>;

type Props = {
  route: ProfileScreenRouteProp;
  navigation: ProfileScreenNavigationProp;
};


type NavigationHeader = {
  tintColor?: string | undefined;
};

const StoresMap: React.FC<Props> = ({ navigation }: Props) => {
  const { colors } = useTheme();
  useEffect(() => {
    navigation.setOptions({
      headerTitle: 'Stores',
      headerRight: ({ tintColor }: NavigationHeader): React.ReactNode => (
        <TouchableOpacity >

        </TouchableOpacity>
      ),
    });
  });
  return (


    <MapView

      provider={PROVIDER_GOOGLE} // remove if not using Google Maps
      style={{ flex: 1 }}
      zoomEnabled={true}
      region={{
        latitude: 22.258,
        longitude: 71.19,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }}
    >
      <Marker
        coordinate={{ latitude: 22.258, longitude: 71.19 }}
        title={"title"}
        description={"test"}

      />
    </MapView>

  );
};

export default StoresMap;