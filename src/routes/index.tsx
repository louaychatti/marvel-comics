import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import CharacterBottomNavigationBar from './character.routes';
import { MainBottomNavigationBar } from './main.routes';
import { useTheme } from '../components/Theme';
import StoresMap from '../pages/StoresMap';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const MainStackNavigator = createStackNavigator();

const Routes: React.FC = () => {
  const { theme , colors } = useTheme();

  return (
    <NavigationContainer theme={theme}>
      <MainStackNavigator.Navigator>
        <MainStackNavigator.Screen
          name="MainTabs"
          component={MainBottomNavigationBar}
          options={{
            headerShown: false,
          }}
        />
        <MainStackNavigator.Screen
          name="CharacterTabs"
          component={CharacterBottomNavigationBar}
          options={{
            headerShown: false,
          }}
        />
        <MainStackNavigator.Screen
          name="StoresMap"
          component={StoresMap}
          options={{
            headerTintColor: '#fff',
            headerTitleAlign: 'center',
            headerStyle: {
              backgroundColor: colors.primary,
            },
          }}
        />
      </MainStackNavigator.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
